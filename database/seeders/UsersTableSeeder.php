<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];

        for ($i=0; $i < 20; $i++) {
            $data[] = [
                'name' => $faker->firstName,
                'phone' => $faker->numerify('900000####'),
                'sms_code' => $faker->numerify('####'),
                'pin_code' => $faker->numerify('####'),
                'email' => $faker->email,
                'password' => Hash::make('12345678'),
                'email_verified_at' => $faker->dateTimeBetween('-1 months', 'now'),
                'created_at' => $faker->dateTimeBetween('-4 months', '- 1 months'),
                'updated_at' => $faker->dateTimeBetween('-4 months', '- 1 months'),
            ];
        }

        DB::table('users')->insert($data);
    }
}

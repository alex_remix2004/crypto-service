<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Faker\Factory as Faker;

class LoginTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /*public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }*/

    public function test_user_can_login_with_correct_credentials()
    {
        $this->withoutExceptionHandling();

        $faker = Faker::create();
        $password = '12345678';
        $phone = $faker->numerify('##########');

        $user = User::factory()->create([
            'name' => $faker->firstName,
            'phone' => $phone,
            'email' => $faker->email,
            'password' => Hash::make($password),
        ]);

        //$this->actingAs($user, 'api');

        // $this->withoutExceptionHandling();

        $response = $this->post('/login', [
            'phone' => $phone,
            'password' => $password,
        ]);

        $response->assertStatus(201);
        //$response->assertRedirect('/');
        //$this->assertAuthenticatedAs($user);
    }
}

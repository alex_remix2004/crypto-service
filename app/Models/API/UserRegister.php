<?php

namespace App\Models\API;

use App\Models\User;
use App\Services\SendSmsService;
use Hash;
use Symfony\Component\HttpFoundation\Response;

/**
 * App\Models\API\UserRegister
 *
 * @property int $status
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|UserRegister newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserRegister newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserRegister query()
 * @mixin \Eloquent
 */
class UserRegister extends User
{
    /**
     * @var int $status
     */
    private int $status;

    /**
     * @param array $data
     * @return int
     */
    public function createNewUser(array $data): int
    {
        try {
            (new User())->create(
                $this->setData($data)
            );
            $this->status = Response::HTTP_CREATED;
        } catch (\Exception $e) {
            //return ['status' => $e->getMessage()];
            $this->status = Response::HTTP_BAD_REQUEST;
        }

         return $this->sendSmsCode($data);
    }

    /**
     * @param $data
     * @return array
     */
    private function setData($data): array
    {
        return [
            'phone' => $data['phone'],
            'sms_code' => $data['sms_code'],
            'password' => Hash::make($data['password'])
        ];
    }

    /**
     * @param $data
     * @return int
     */
    private function sendSmsCode($data): int
    {
        if ($this->status == Response::HTTP_CREATED) {
            //$this->status = (new SendSmsService($data));
        }

        return $this->status;
    }
}

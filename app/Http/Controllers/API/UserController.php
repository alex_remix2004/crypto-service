<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\UserRegisterRequest;
use App\Models\API\UserRegister;
use App\Models\User;
use App\Services\SendSmsService;
use Illuminate\Http\Request;

class UserController extends AbstractController
{
    /**
     * @param UserRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserRegisterRequest $request)
    {
        $data = $request->input();
        $data['sms_code'] = substr(str_shuffle("0123456789"), 0, 4);

        $status = (new UserRegister())->createNewUser($data);

        return response()->json(['status' => $status], $status);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $data = $request->input();
        $credentials = [
            'phone' => $data['phone'],
            'code' => $data['code'],
        ];

        $token = auth()->attempt($credentials);

        return response()->json(['login' => $credentials], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd(__METHOD__);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd(__METHOD__);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        $result = User::destroy($id); # soft-delete using

        if ($result) {
            return response()->json(['status' => 'deleted'], 200);
        }
    }
}

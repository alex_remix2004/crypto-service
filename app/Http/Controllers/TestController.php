<?php

namespace App\Http\Controllers;

use App\Models\User;

class TestController extends Controller
{
    public function hello()
    {
        return response()->json(User::get(), 200);
    }
}

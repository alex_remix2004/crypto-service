<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;

class AppKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('APP_KEY');
        if ($token !== config('app.api_secret.key')) {
            return response()->json(['message' => 'App key is invalid'], 401);
        }

        return $next($request);
    }
}

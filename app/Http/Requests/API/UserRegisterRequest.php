<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','max:10'],
            'sms_code' => ['nullable', 'min:4', 'max:4'],
            'password' => ['required', 'string', 'min:8'],
        ];
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'phone.required' => 'Номер обязательное поле',
            'password.required'  => 'Пароль обязательное поле',
            'phone.min:10'  => 'Количество цифр в номере = 10',
            'phone.max:10'  => 'Количество цифр в номере = 10',
        ];
    }
}

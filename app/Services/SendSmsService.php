<?php


namespace App\Services;


use App\Http\Controllers\Controller;
use Nutnet\LaravelSms\Contracts\Provider;
use Nutnet\LaravelSms\SmsSender;
use Symfony\Component\HttpFoundation\Response;

class SendSmsService
{
    const CODE_RUS = "7";
    const SMS_MESSAGE = "Ваш код: ";
    private $phone;
    private $sms_code;

    public function __construct(array $data)
    {
        $this->phone = $data['phone'];
        $this->sms_code = $data['sms_code'];
        $this->sendSms();
    }

    public function sendSms(\Nutnet\LaravelSms\SmsSender $smsSender)
    {
        $smsSender->send(self::CODE_RUS . $this->phone, self::SMS_MESSAGE . $this->sms_code);

        return Response::HTTP_OK;
    }
}

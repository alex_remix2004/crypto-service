<?php


namespace App\Services;

/**
 * Class LoginAttribute getting attribute for login action
 * @package App\Actions
 */
class LoginAttribute
{
    /**
     * @param string $login
     * @return string
     */
    public function detectAttribute(string $login): string
    {
        if(is_numeric($login)){
            $field = 'phone';
        } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } else {
            $field = 'name';
        }

        request()->merge([$field => $login]);

        return $field;
    }
}

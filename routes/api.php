<?php

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\TestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hello', [TestController::class, 'hello'])->name('hello');

Route::middleware(['return-json:api'])->group( function () {
    Route::post('/register', [UserController::class, 'register'])->name('api.users.register');
    Route::post('/login', [UserController::class, 'login'])->name('api.users.login');
});

Route::group(['namespace' => 'App\Http\Controllers\API', 'prefix' => 'users'], function() {
    $methods = ['show', 'edit', 'update', 'destroy'];
    Route::resource('model', 'UserController')
        ->only($methods)
        ->names('api.users.model');

    /*Route::resource('pages', 'PageController')
        ->except(['show'])
        ->names('admin.pages.model');*/
});
